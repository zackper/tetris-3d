using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InputManager : MonoBehaviour
{
    private Dictionary<string, UnityEvent> buttonMap = new Dictionary<string, UnityEvent>();

    private void Start()
    {
        buttonMap.Add("space", new UnityEvent());
        buttonMap.Add("left", new UnityEvent());
        buttonMap.Add("right", new UnityEvent());
        buttonMap.Add("up", new UnityEvent());
        buttonMap.Add("down", new UnityEvent());
        buttonMap.Add("rotateY", new UnityEvent());
        buttonMap.Add("rotateZ", new UnityEvent());

        foreach (var item in buttonMap)
            Subscribe(item.Key, IncreaseSystemInputs);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            buttonMap["space"].Invoke();
        if (Input.GetKeyDown(KeyCode.A))
            buttonMap["left"].Invoke();
        if (Input.GetKeyDown(KeyCode.D))
            buttonMap["right"].Invoke();
        if (Input.GetKeyDown(KeyCode.W))
            buttonMap["up"].Invoke();
        if (Input.GetKeyDown(KeyCode.S))
            buttonMap["down"].Invoke();
        if (Input.GetKeyDown(KeyCode.Q))
            buttonMap["rotateY"].Invoke();
        if (Input.GetKeyDown(KeyCode.E))
            buttonMap["rotateZ"].Invoke();
    }
    
    public void Subscribe(string buttonName, UnityAction callback)
    {
        Debug.Assert(buttonMap.ContainsKey(buttonName));
        buttonMap[buttonName].AddListener(callback);
    }
    public void IncreaseSystemInputs()
    {
        SystemInformation.Instance.systemValues["totalInputs"] += 1;
        SystemInformation.Instance.CallUpdate();
    }


    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Singleton Variables

    public static InputManager Instance { get; private set; }
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(this);
    }
}
