using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(TextMeshProUGUI))]
public class UIGlobalValue : MonoBehaviour
{
    [SerializeField]
    private string id; // Use this id to gain value from singleton

    private TextMeshProUGUI text;

    private void Start()
    {
        // Get reference to text
        text = GetComponent<TextMeshProUGUI>();

        // Subscribe to update event
        SystemInformation.Instance.SubscribeUpdate(UpdateValue);

        // Get Initial value
        if (SystemInformation.Instance.systemValues.ContainsKey(id))
            text.text = SystemInformation.Instance.systemValues[id].ToString();
        else
            Debug.Assert(false, "Wrong id used..." + id);
    }

    private void UpdateValue()
    {
        // Just update the value from the singleton
        text.text = SystemInformation.Instance.systemValues[id].ToString();
    }
}
