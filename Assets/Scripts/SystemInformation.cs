using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public sealed class SystemInformation : MonoBehaviour
{
    public Dictionary<string, int> systemValues;
    private UnityEvent valuesUpdateEvent = new UnityEvent();

    private void Start()
    {
        systemValues = new Dictionary<string, int>();
        systemValues.Add("totalBlocks", 0);
        systemValues.Add("brokenBlocks", 0);
        systemValues.Add("destroyedBlocks", 0);
        systemValues.Add("totalInputs", 0);
        systemValues.Add("combos", 0);
        systemValues.Add("score", 0);
    }

    public void SubscribeUpdate(UnityAction callback)
    {
        valuesUpdateEvent.AddListener(callback);
    }
    public void CallUpdate()
    {
        valuesUpdateEvent.Invoke();
    }

    public void Reset()
    {
        systemValues = new Dictionary<string, int>();
    }

    public static SystemInformation Instance { get; private set; }
    private void Awake()
    {
        // If there is an instance, and it's not me, delete myself.

        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }
}