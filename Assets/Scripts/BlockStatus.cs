using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockStatus : MonoBehaviour
{
    private bool isWhole = true;
    private int totalBlocks = -1;

    private void Start()
    {
        totalBlocks = transform.childCount;
    }

    public bool IsWhole
    {
        get
        {
            return isWhole; 
        }
    }

    public void Reduce()
    {
        if (IsWhole)
            Break();

        totalBlocks--;
        if(totalBlocks == 0)
        {
            SystemInformation.Instance.systemValues["destroyedBlocks"] += 1;
            SystemInformation.Instance.CallUpdate();
        }
    }
    public void Break()
    {
        if (IsWhole == false)
            Debug.Assert(false, "This should only be set once");

        isWhole = false;

        // Update singleton information
        SystemInformation.Instance.systemValues["brokenBlocks"] += 1;
        SystemInformation.Instance.CallUpdate();
    }
}
