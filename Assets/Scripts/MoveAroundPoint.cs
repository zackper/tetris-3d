using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAroundPoint : MonoBehaviour
{
    [SerializeField]
    private float mouseSensitivity;

    private float rotationY;
    private float rotationX;

    private Transform target;
    private float distanceFromTarget = 20;

    private void Start()
    {
        target = GameObject.FindGameObjectWithTag("CenterPoint").GetComponent<Transform>();

        rotationX = 20;
        rotationY = 75;
        transform.localEulerAngles = new Vector3(rotationX, rotationY, 0);
        transform.position = target.position - transform.forward * distanceFromTarget;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            if(transform.localEulerAngles == new Vector3(90, 90, 0))
                transform.localEulerAngles = new Vector3(rotationX, rotationY, 0);
            else
                transform.localEulerAngles = new Vector3(90, 90, 0);
            transform.position = target.position - transform.forward * distanceFromTarget;
            return;
        }
        if (Input.GetMouseButton(0)) // Change rotation values if user wants it
        {
            float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity;
            float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity;

            rotationY += mouseX;
            rotationX -= mouseY;

            rotationX = Mathf.Clamp(rotationX, 5, 90);
            rotationY = Mathf.Clamp(rotationY, 20, 120);

            transform.localEulerAngles = new Vector3(rotationX, rotationY, 0);
            transform.position = target.position - transform.forward * distanceFromTarget;
        }
    }
}
