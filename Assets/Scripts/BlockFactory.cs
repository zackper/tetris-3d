using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockFactory : MonoBehaviour
{
    public GameObject[] blockPrefabs;
    public Material[] materials;

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Initializers

    private void Start()
    {
        materials       = Resources.LoadAll<Material>("BlockMaterials");
        blockPrefabs    = Resources.LoadAll<GameObject>("BlockVariants");
    }
    private Material GetRandomMaterial()
    {
        return materials[Random.Range(0, materials.Length)];
    }
    private GameObject GetRandomBlockPrefab()
    {
        return blockPrefabs[Random.Range(0, blockPrefabs.Length)];
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Main Public function for random blocks

    public List<GameObject> SpawnBlock(Vector3Int position, Transform parent = null)
    {
        List<GameObject> block = new List<GameObject>();

        // Get random block prefab
        GameObject blockPrefab = GetRandomBlockPrefab();
        // Get random material for block
        Material material = GetRandomMaterial();

        GameObject blockInstance = Instantiate(blockPrefab);
        blockInstance.transform.position = position;
        if (parent != null)
            blockInstance.transform.SetParent(parent);

        for(int i = 0; i < blockInstance.transform.childCount; i++)
        {
            // Add cube to list
            GameObject cube = blockInstance.transform.GetChild(i).gameObject;
            block.Add(cube);
            // Set Material
            cube.GetComponent<Renderer>().material = material;
        }

        return block;
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Util

    public GameObject SpawnCube(Vector3Int position)
    {
        GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube.transform.position = position;
        return cube;
    }
}
