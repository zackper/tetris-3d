using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

[RequireComponent(typeof(BlockFactory))]
public class GridManager : MonoBehaviour
{
    [SerializeField]
    private int width = 6, length = 6, height = 9;
    [SerializeField]
    private float roundTime = 1f;

    // As cubes are added, fill in the completion matrix to make faster checks.
    private bool[][][] completionMatrix;
    // Block factory class will be used to spawn new falling blocks
    private BlockFactory blockFactory;

    // List of cubes falling 
    private List<GameObject> fallingBlock;
    // List of cubes representing the next block that will spawn
    private List<GameObject> nextBlock;
    // List of all the placed cubes in the grid
    private List<GameObject> cubes = new List<GameObject>();

    // List of cubes representing the floor of the grid (for coordination assistance)
    private List<GameObject> floorCubes = new List<GameObject>();

    // List of currently used indicator cubes
    private GameObject indicatorCubePrefab;
    private List<GameObject> indicatorCubes = new List<GameObject>();

    // Reference to parent object of next block
    private GameObject nextBlockParent;

    private void Start()
    {
        // Get reference of block factory
        blockFactory = GetComponent<BlockFactory>();

        // Register callbacks for input
        RegisterInputCallbacks();

        // Create the map for coordination refernece
        CreateMap();

        // Invoke repeatable progress function
        StartProgress();

        // Load prefabs
        indicatorCubePrefab = Resources.Load<GameObject>("Other/IndicatorCube");

        // Initialize next block
        NextBlock_Initialize();

        // Initialize completion matrix for faster calculations
        completionMatrix = new bool[width][][];
        for (int i = 0; i < width; i++)
        {
            completionMatrix[i] = new bool[height][];
            for (int j = 0; j < height; j++)
            {
                completionMatrix[i][j] = new bool[length];
                for (int k = 0; k < length; k++)
                    completionMatrix[i][j][k] = false;
            }
        }
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Main Logic Loop

    private void Progress()
    {
        if (FallingBlock_Exists())
            FallingBlock_Progress();
        else
        {
            FallingBlock_SpawnNext();
            NextBlock_SpawnNext();
        }

        CheckGridCompletion();  // Check for completion before indicator
        ShowIndicator();        // Show indicator of falling block
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Falling Block List<> functions

    private void FallingBlock_SpawnNext()
    {
        SystemInformation.Instance.systemValues["totalBlocks"] += 1;
        SystemInformation.Instance.CallUpdate();

        fallingBlock = nextBlock;

        // Use local position to keep relative positions of each cube and get them to center
        foreach (GameObject cube in fallingBlock)
            cube.transform.position = cube.transform.localPosition + new Vector3(width / 2, height - 2, length / 2);

        foreach (GameObject cube in fallingBlock)
        {
            Vector3Int pos = Vector3Int.FloorToInt(cube.transform.position);
            if (completionMatrix[pos.x][pos.y][pos.z] == true)
                GameOver();
        }
    }
    private bool FallingBlock_Exists()
    {
        return fallingBlock != null;
    }
    private bool FallingBlock_IsFalling()
    {
        return fallingBlock.Count > 0;
    }
    private void FallingBlock_Progress()
    {
        Debug.Assert(FallingBlock_Exists(), "To progress, falling block should exist");
        FallingBlock_CheckIsFalling();

        if (FallingBlock_Exists() == false) // It is possible that the block has reached a grounded state and it got deleted.
            return;

        foreach (GameObject cube in fallingBlock) // Assumes that cube is already aligned with grid
        {
            cube.transform.position -= new Vector3Int(0, 1, 0);
        }
    }
    private void FallingBlock_Move(Vector3 dPosition)
    {
        Debug.Assert(dPosition.x <= 1);
        Debug.Assert(dPosition.y == 0);
        Debug.Assert(dPosition.z <= 1);

        bool isMoveAllowed = true;
        foreach (GameObject cube in fallingBlock)
        {
            Vector3 targetPosition = cube.transform.position + dPosition;

            // Check grid boundaries
            if (
                targetPosition.x < 0 ||
                targetPosition.x > width - 1 ||
                targetPosition.z < 0 ||
                targetPosition.z > length - 1
            )
                isMoveAllowed = false;

            // Check other placed cubes collisions
            foreach (GameObject placedCube in cubes)
                if (targetPosition == placedCube.transform.localPosition)
                    isMoveAllowed = false;
        }
        if (isMoveAllowed)
            foreach (GameObject cube in fallingBlock)
                cube.transform.position += dPosition;

        ShowIndicator();
    }
    private void FallingBlock_Rotate(Vector3 rotationAxis)
    {
        // First find middle points for each axis
        Vector3 lowest = new Vector3(width, height, length);
        Vector3 highest = new Vector3(-1, -1, -1);
        foreach (GameObject cube in fallingBlock)
        {
            // Find lowest coords
            if (cube.transform.position.x < lowest.x)
                lowest.x = cube.transform.position.x;
            if (cube.transform.position.y < lowest.y)
                lowest.y = cube.transform.position.y;
            if (cube.transform.position.z < lowest.z)
                lowest.z = cube.transform.position.z;
            // Find heighest coordsa
            if (cube.transform.position.x > highest.x)
                highest.x = cube.transform.position.x;
            if (cube.transform.position.y > highest.y)
                highest.y = cube.transform.position.y;
            if (cube.transform.position.z > highest.z)
                highest.z = cube.transform.position.z;
        }

        // Find middle point (Special cases for tetris rotation logic)
        Vector3 middle = new Vector3(
            lowest.x + (highest.x - lowest.x) / 2,
            lowest.y + (highest.y - lowest.y) / 2,
            lowest.z + (highest.z - lowest.z) / 2
        );

        bool isXOnBlock = (middle.x - Mathf.Floor(middle.x)) == 0;
        bool isYOnBlock = (middle.y - Mathf.Floor(middle.y)) == 0;
        bool isZOnBlock = (middle.z - Mathf.Floor(middle.z)) == 0;

        // Store previous positions. 
        Vector3[] previousPositions = new Vector3[fallingBlock.Count];
        for (int i = 0; i < fallingBlock.Count; i++)
            previousPositions[i] = fallingBlock[i].transform.position;

        // Rotate cubes around middle point
        if (rotationAxis.y == 1)
        {
            // Then I care for middle of x and z
            if (isXOnBlock == isZOnBlock)   // Normal rotate around middle point
                ;
            else                            // Approxiamate new middle point that falls on block
            {
                middle.x = Mathf.Floor(middle.x);
                middle.z = Mathf.Floor(middle.z);
            }
            foreach (GameObject cube in fallingBlock)
            {
                cube.transform.RotateAround(middle, rotationAxis, 90);
            }
        }
        else if (rotationAxis.z == 1)
        {
            // Then I care for middle of x and z
            if (isXOnBlock == isYOnBlock)   // Normal rotate around middle point
                ;
            else                            // Approxiamate new middle point that falls on block
            {
                middle.x = Mathf.Floor(middle.x);
                middle.y = Mathf.Floor(middle.y);
            }
            foreach (GameObject cube in fallingBlock)
            {
                cube.transform.RotateAround(middle, rotationAxis, 90);
            }
        }

        // Check if cubes dont fall into already captures blocks
        bool isMoveAllowed = true;
        for(int i = 0; i < fallingBlock.Count; i++)
        {
            Vector3Int position = Vector3Int.FloorToInt(fallingBlock[i].transform.position);
            Debug.Log(position);
            if(
                position.x < 0 || position.x > width - 1 ||
                position.y < 0 || position.y > height - 1 ||
                position.z < 0 || position.z > length - 1 ||
                completionMatrix[position.x][position.y][position.z] == true
            )
            {
                Debug.Log("Not allowed!");
                isMoveAllowed = false;
                break;
            }
        }

        // If move is not allowed revert back to original position
        if (isMoveAllowed == false)
            for (int i = 0; i < fallingBlock.Count; i++)
                fallingBlock[i].transform.position = previousPositions[i];
        

        ShowIndicator();
    }
    private void FallingBlock_CheckIsFalling()
    {
        bool isFalling = true;
        foreach (GameObject cube in fallingBlock)
        {
            if (Equals(cube.transform.position.y, 0))                               // Either when it hits the ground
            {
                isFalling = false;
                break;
            }
            else
            {
                Vector3Int pos = Vector3Int.FloorToInt(cube.transform.position);    // Or when its bottom cell is completed
                if (completionMatrix[pos.x][pos.y - 1][pos.z] == true)
                {
                    isFalling = false;
                    break;
                }
            }
        }

        if (isFalling == false)
        {
            foreach (GameObject cube in fallingBlock)
            {
                cubes.Add(cube);
                Vector3Int pos = Vector3Int.FloorToInt(cube.transform.position);
                completionMatrix[pos.x][pos.y][pos.z] = true;
            }

            fallingBlock = null;
        }
    }
    private void FallingBlock_ImmediateDrop()
    {
        StopProgress();

        // Find initial and final position
        while (FallingBlock_Exists() && FallingBlock_IsFalling())
        {
            FallingBlock_CheckIsFalling();
            if (FallingBlock_Exists() == false)
                break;
            else
                foreach (GameObject cube in fallingBlock)
                    cube.transform.position -= new Vector3(0, 1f, 0);
        }

        StartProgress();
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Next Block functions
    private void NextBlock_Initialize()
    {
        nextBlockParent = new GameObject("NextBlockViewer");
        nextBlockParent.transform.SetParent(this.transform);
        nextBlockParent.transform.position = new Vector3(width / 2, 4, length + 4);

        NextBlock_SpawnNext();
    }
    private void NextBlock_SpawnNext()
    {
        // Assert that previous block has been removed.
        Debug.Assert(nextBlockParent.transform.childCount == 0, "Cant spawn next if previous has not been removed");
        nextBlock = blockFactory.SpawnBlock(Vector3Int.FloorToInt(nextBlockParent.transform.position), this.transform);
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Util functions

    // Initializer functions
    private void CreateMap()
    {
        GameObject centerPoint = new GameObject("CenterPoint");
        centerPoint.tag = "CenterPoint";
        centerPoint.transform.position = new Vector3((float)width / 2f, (float)height / 4f, (float)length / 2f);
        centerPoint.transform.SetParent(this.transform);

        // Spawn floor tiles
        for (int w = 0; w < width; w++)
        {
            for (int l = 0; l < length; l++)
            {
                Vector3Int pos = new Vector3Int(w, -1, l);
                GameObject floorCube = blockFactory.SpawnCube(pos);
                floorCube.transform.SetParent(centerPoint.transform);
                floorCube.transform.localScale = new Vector3(0.9f, 1f, 0.9f);
                floorCubes.Add(floorCube);
            }
        }
    }
    private void RegisterInputCallbacks()
    {
        InputManager.Instance.Subscribe("left", () =>
        {
            if (FallingBlock_Exists())
                FallingBlock_Move(new Vector3(0, 0, 1));
        });
        InputManager.Instance.Subscribe("right", () =>
        {
            if (FallingBlock_Exists())
                FallingBlock_Move(new Vector3(0, 0, -1));
        });
        InputManager.Instance.Subscribe("up", () =>
        {
            if (FallingBlock_Exists())
                FallingBlock_Move(new Vector3(1, 0, 0));
        });
        InputManager.Instance.Subscribe("down", () =>
        {
            if (FallingBlock_Exists())
                FallingBlock_Move(new Vector3(-1, 0, 0));
        });
        InputManager.Instance.Subscribe("rotateY", () =>
        {
            if (FallingBlock_Exists())
                FallingBlock_Rotate(new Vector3(0f, 1f, 0f));
        });
        InputManager.Instance.Subscribe("rotateZ", () =>
        {
            if (FallingBlock_Exists())
                FallingBlock_Rotate(new Vector3(0f, 0f, 1f));
        });
        InputManager.Instance.Subscribe("space", () =>
        {
            FallingBlock_ImmediateDrop();
        });
    }

    // Control progress function
    private void StopProgress()
    {
        CancelInvoke("Progress");
    }
    private void StartProgress()
    {
        InvokeRepeating("Progress", 0, roundTime);
    }
    private void GameOver()
    {
        // I got tired so I did this part dirty

        // Get Final score label and append score
        TextMeshProUGUI finalScore = GameObject.FindGameObjectWithTag("PlayerScoreTMP").GetComponent<TextMeshProUGUI>();
        finalScore.text = "Your score: " + SystemInformation.Instance.systemValues["score"];

        // Find canvas and trigger gameover on animator
        GameObject.FindGameObjectWithTag("Canvas").GetComponent<Animator>().SetTrigger("gameover");

        StopProgress();
    }
    public void PlayAgain()
    {
        SystemInformation.Instance.Reset();
        SceneManager.LoadScene("Tetris 3D");
    }

    // Util function to find the heighest unused tile
    private Vector3 FindHeighestFreeTile(int x, int z)
    {
        for (int i = 0; i < height; i++)
        {
            if (completionMatrix[x][i][z] == false)
                return new Vector3(x, i, z);
        }

        // It will never reach this point, the player will lose
        return new Vector3(-1, -1, -1);
    }

    // Functions that help with the aesthetic of the game
    private void ShowIndicator()
    {
        if (FallingBlock_Exists() == false)
            return;

        // Optimize indicators to be spawned once only
        int usedIndicators = 0;

        for (int i = 0; i < fallingBlock.Count; i++)
        {
            GameObject cube = fallingBlock[i];
            Vector3 dropPoint = FindHeighestFreeTile((int)cube.transform.position.x, (int)cube.transform.position.z);

            // if there are not enough indicator cubes, spawn next
            if (indicatorCubes.Count < i + 1)
                indicatorCubes.Add(Instantiate(indicatorCubePrefab, this.transform));

            indicatorCubes[i].SetActive(true);
            indicatorCubes[i].transform.position = dropPoint + new Vector3(0, 0.0001f, 0); // Add upwards margin to avoid overlap
            usedIndicators = i;
        }

        // Hide unused indicators
        for (int i = usedIndicators + 1; i < indicatorCubes.Count; i++)
        {
            indicatorCubes[i].SetActive(false);
        }
    }

    // Util functions to compare doubles with tolerance
    private bool Equals(double x, double y)
    {
        if (Mathf.Abs((float)x - (float)y) < 0.0001)
            return true;
        else
            return false;
    }
    private bool Equals(Vector3 a, Vector3 b)
    {
        if (
            Mathf.Abs(a.x - b.x) < 0.0001 &&
            Mathf.Abs(a.y - b.y) < 0.0001 &&
            Mathf.Abs(a.z - b.z) < 0.0001
        )
            return true;
        else
            return false;
    }

    // Layer manipulation functions
    private void CheckGridCompletion()
    {
        int completedLayers = 0;        // Keep completed layers for scoring
        for (int h = height - 1; h >= 0; h--)
        {
            if (IsLayerCompleted(h))
            {
                ++completedLayers;
                DestroyLayer(h);
            }
        }

        if(completedLayers > 0)
        {
            SystemInformation.Instance.systemValues["score"] += (width * length * completedLayers);
        }
        if(completedLayers >= 2)
        {
            SystemInformation.Instance.systemValues["combos"] += completedLayers;
        }
    }
    private bool IsLayerCompleted(int h)
    {
        for (int w = 0; w < width; w++)
        {
            for (int l = 0; l < length; l++)
            {
                // If at least one is not filled, return false
                if (completionMatrix[w][h][l] == false)
                {
                    return false;
                }
            }
        }

        return true;
    }
    private void DestroyLayer(int h)
    {
        Stack<GameObject> toRemove = new Stack<GameObject>();
        foreach (GameObject cube in cubes)
        {
            if (cube.transform.position.y == h)
            {
                Vector3Int pos = Vector3Int.FloorToInt(cube.transform.position);
                completionMatrix[pos.x][pos.y][pos.z] = false;
                toRemove.Push(cube);
            }
        }

        while (toRemove.Count > 0)
        {
            GameObject cube = toRemove.Pop();

            BlockStatus blockStatus = cube.transform.parent.GetComponent<BlockStatus>();
            blockStatus.Reduce();

            cubes.Remove(cube);
            Destroy(cube);
        }

        // Drop all cubes above h one layer down
        foreach (GameObject cube in cubes)
            if (cube.transform.position.y > h)
                cube.transform.position -= new Vector3(0, 1, 0);

        // Push all completion values one layer down
        for (int h2 = h; h2 < height; h2++)
            for (int w = 0; w < width; w++)
                for (int l = 0; l < length; l++)
                {
                    if (h2 + 1 == height) // Top level, just set completion to false
                        completionMatrix[w][h2][l] = false;
                    else
                        // Get completion value of above layer
                        completionMatrix[w][h2][l] = completionMatrix[w][h2 + 1][l];
                }
    }
}